<?php
#| Constant -->

$name = 'Sarah';
$name = 'Dan';

// !define(string $name , mixed $value [, bool $case_insensitive)
define('NAME', 'John'); 
// const NAME = 'John'; 
define('AGE', 27);
// const AGE = 27;
define('EXIST', true); //return 1, because True

echo NAME . ' ' . AGE . "</br>"; 
echo $name . ' ' . EXIST . "</br>" ; 

##| Magic Constant
echo __DIR__ . "</br>";
echo __FILE__ . "<hr>";

#| Operators -->

// !Arithmetic Operators
$a = 3;
$b = 5;

var_dump($a + $b);
var_dump($a - $b);
var_dump($a * $b);
var_dump($a / $b);
var_dump($a % $b); //Remainder of $a divided by $b
var_dump($a ** $b);

$c = 1.6;
$d = (int) $c; //Type Operators
var_dump($d);

$x = 3 * 3 % 5; // (3 * 3) % 5 = 4

$num_1 = 0.0; 
$num_2 = (bool) $num_1 ; 
var_dump($num_2); // 0 and 0.0 ... is false

$num_3 = 1;
$num_3++; // $num_3 = $num_3 + 1
++$num_3;

$num_4 = $num_3++; //3
# $num_4 = ++$num_3; //4
var_dump($num_3);
var_dump($num_4);
echo "<hr>";


// !Comparison Operators
// ?answer is true or false

$a = 1;
$b = "1";
$c = 2;
var_dump($a == $b); //Equal
var_dump($a === $b); //Identical 

// * $a != $b || $a <> $b Not equal

var_dump($a < $c);
var_dump($a > $c);
var_dump($a <= $c);
var_dump($a >= $c);

// SpaceShip
echo 1 <=> 1; // 0
echo 1 <=> 2; // -1
echo 2 <=> 1; // 1
echo '<hr>';

// !Logical Operators
// ?answer is true or false

$x = true;
$y = false;

//* and or &&; (and)
//* or or ||; (or)
//* !$x mean x (not) true is false; (not)
//* Xor  if either $a or $b is true, 'but not both'.

// !Assignment Operators
// = += -= *= **= /= .= %= &= |= ^= <<= >>= ??=

$a = 6;
$a += 5; // $a = $a + 5
# $a -= 5; // $a = $a - 5

$b = $a + 4; // or $b = ($a = 5) + 4;
var_dump($a);
var_dump($b);
echo '<hr>';

# Control Structures -->
// !if, else and elseif

$isLogin = true;

if(!$isLogin){
  echo 'User panel';
} else{
  echo 'Please login!';
}
echo "</br>";

$glass = 'red';

if($glass == 'red'){
  echo 'This is red glass';
} elseif($glass == 'blue'){
  echo 'This is blue glass';
} else{
  echo 'Please pickup a glass';
}
echo "</br>";

$a = 2;
$b = 5;

// use ternary Operator for one line conditions
$a > $b ? var_dump("a is bigger than b") : var_dump("b is bigger than a") ;
echo '<hr>';

// !switch

$car = 'blue';

switch ($car) {
  case 'red':
     echo 'This is red car';
    break;
  case 'blue':
     echo 'This is blue car';
    break;
  default : 
     echo 'Choose a car';
}
echo "</br>";

switch ($i) {
  case 0:
  case 1: 
  case 2: 
    echo 'i is less than 3';
    break;
  case 3:  
    echo 'i is 3';
    break; 
  }

echo '<hr>';


// !while and doWhile 

$list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] ;

// *While
$i = 0;

while ($i < 10) {

  echo $list[$i] .'<br>';

  $i++;
}

// *doWhile
$a = 0;

do {
  echo $list[$a] .'<br>';

  $a++;
} while ($a > 10); // false condition
echo '<hr>';

// !for and foreach

// *for

for ($index = 10; $index >= 0; $index--) { 
  echo $list[$index] .'<br>';
}

// *foreach: loop in Array

$users = ['Batman', 'WanderWoman', 'Superman', 'Flash', 'Cyborg'];

foreach ($users as $key => $user) {
  // echo $user .'<br>';
  echo "key $key : $user <br>";
}
echo '<hr>';


// !Break and continue

foreach ($users as $hero) {
  if ($hero == 'Superman') break;
  echo $hero . '<br>';
}
echo "</br>";

foreach ($users as $fun) {
  if ($fun == 'Flash') continue;
  echo "$fun <br>";
}
echo '<hr>';
// !match

$car = 'blue';
$color = null;

// switch ($car) {
//   case 'red':
//      echo 'This is red car';
//     break;
//   case 'blue':
//      echo 'This is blue car';
//     break;
//   default : 
//      echo 'Choose a car';
// }

echo match($car){
  'red' => 'This is red car',
  'blue' => 'This is blue car',
  default => 'Choose a car'
}


?>
